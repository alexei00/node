/* ADD MORE TWITTER IDS HERE */

var accounts_Twitter = [
    /// Banco Azteca
    '187552996', // @BancoAzteca 
    '637362286', // @BBVABancomer
    '3494032572', // @Banorte_mx
    '266108022', // @BanCoppel (No Oficial)
    //'126356931',           // @CentroCitibnmx
    '92367652', //@Citibabamex
    '2321138629', // @BancoFamsa
    '131024114', // @CompartamosBanc
    '2644850838', // @HSBC_MX
    '205452984', // @SantanderMx
    '187646961', // @BancoInbursa
    '376777934', // @BanorteEscucha
    /// Elektra
    '79264550', // @Tiendas_Elektra
    '2910407244', // @amazonmex
    '65392033', // @liverpoolmexico
    '112775424', // @Coppel
    '150757675', // @WalmartMexico
    '166197360', // @Chedrauioficial
    '4204177874', // @LaComerOficial
    '40002725', // @palaciohierro
    '35249554', // @Famsa
    //Tim Hortons
    '830129697451343872', // @Tim Hortons MexicoCuenta verificada
    '130907918', //Starbucks Mexico
    '119545998', //KrispyKreme Mexico
    '2992992479', //@DunkinDonutsmx
    '36848357', //@McDonaldsMexico,
    '279656664', //@CarlsJrMx,
    '119439182', //@Tiendas_OXXO
    '125243432' //7ElevenMexico
];

module.exports = {
    accounts_Twitter
};