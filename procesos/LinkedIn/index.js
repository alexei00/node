'use strict';
const { app, BrowserWindow } = require('electron');
var mssql = require('../Database/util');
const jsdom = require('jsdom');
const request = require('request');
const _ = require('lodash');
const async = require('async');
var moment = require('moment');
require('electron-debug')();

var result_final = [];
var accounts_LinkedIn = require('../Accounts/accounts_LinkedIn.js');
var date_select = moment().format("YYYY-MM-DD");



const startScrape = (url) => {
    return Promise.resolve({ url, result: {} })
};

const createWindow = obj => new Promise((resolve, reject) => {
    let win
    win = new BrowserWindow({ width: 800, height: 600 })
    win.loadURL(obj.url)
    win.on('closed', () => {
        win = null
    });
    return resolve(_.merge(obj, { win }))
});

const getPathname = obj => new Promise((resolve, reject) => {
    // on browser load finish
    obj.win.webContents.once('did-finish-load', () => {
        // execute js to get pathname
        obj.win.webContents.executeJavaScript('location.pathname', pathname => {
            return resolve(_.merge({ pathname }, obj))
        });
    });
});

const getFollowers = obj => new Promise((resolve, reject) => {
    let querySelector = '';
    if (obj.pathname.indexOf('/company-beta/') !== -1 || obj.pathname.indexOf('/company/') !== -1) {
        querySelector = '.org-top-card-module__followers-count';
        if (querySelector) {
            setTimeout(() => {
                obj.win.webContents.executeJavaScript(`document.querySelector('${querySelector}').innerText`, text => {
                    // add followers to results
                    obj.result.followers = parseInt(text.match(/\d/g).join(''));
                    return resolve(obj)
                })
            }, 4000);
        }
    }
})

const getCompanyId = obj => new Promise((resolve, reject) => {
    let companyId = parseInt(obj.pathname.match(/\d/g) ? obj.pathname.match(/\d/g).join('') : NaN);
    if (!companyId) {
        setTimeout(() => {
            obj.win.webContents.executeJavaScript('parseInt(document.body.innerHTML.match(/companyId=\\d+/g)[0].match(/\\d+/g)[0])', id => {
                if (id) {
                    return resolve(_.merge(obj, { companyId: id }))
                } else {
                    return reject(new Error('No Company Id Found.'))
                }
            })
        }, 4000)
    } else {
        return resolve(_.merge(obj, { companyId }))
    }
})

const closeWindow = obj => new Promise((resolve, reject) => {
    obj.win.close()
    resolve(obj)
});

app.on('ready', () => {
    return new Promise(function(resolve, reject) {
        async.eachSeries(accounts_LinkedIn.accounts_LinkedIn, function(url, cb) {
            console.log('_____________________________________________________');
            var url_lin = "https://www.linkedin.com/company/" + url.id;
            console.log('*Getting data from:', url.nombre);
            startScrape(url_lin)
                .then(createWindow)
                .then(getPathname)
                .then(getFollowers)
                .then(getCompanyId)
                .then(closeWindow)
                .then(obj => {
                    var payload = {};
                    payload.followers = obj.result.followers;
                    payload.CompanyId = obj.companyId;
                    payload.name = url.nombre;
                    var query = "IF EXISTS (SELECT * FROM LinkedInAnalytics.dbo.Company_Insights WHERE Company_id = " + payload.CompanyId + " AND DATE='" + date_select + "') update LinkedInAnalytics.dbo.Company_Insights set Followers=" + payload.followers + " WHERE Company_id = " + payload.CompanyId + " AND DATE='" + date_select + "';  ELSE INSERT INTO LinkedInAnalytics.dbo.Company_Insights (Date,Company_name,Company_id,Followers) values ('" + date_select + "','" + payload.name + "'," + payload.CompanyId + "," + payload.followers + ");";
                    payload.query = query;
                    result_final.push(payload);
                    cb();
                });
        }, function() {
            console.log('Extraction Finish');
        });
    });

});





function mssqlInsertData(payload) {
    return new Promise(function(resolve, reject) {
        async.eachSeries(payload, function(row, cb) {
            var q = mssql.getQuery(row.query).then(function(data) {
                console.log(row.query);
                cb();
            });
        }, function() {
            app.quit();
            resolve();
        });
    });
};




app.on('window-all-closed', () => {
    mssqlInsertData(result_final).then(function() {
        console.log("Insertion Finish");
        app.quit();
    });
});