/************** DATABASE SQL SERVER *********/
var sql = require('mssql');
var Promise = require('bluebird');

var config = {
    user: 'RC_Admin',
    password: 'Andres2345Angel',
    server: '144.202.2.213'
};

function getQuery(query) {
    return new Promise(function(resolve, reject) {
        //console.log(query);
        var conn = new sql.ConnectionPool(config);
        conn.connect().then(function() {
                var req = new sql.Request(conn);
                req.query(query).then(function(recordset) {
                        conn.close();
                        resolve(recordset);
                    })
                    .catch(function(err) {
                    	console.log(err);
                        conn.close();
                    });
            })
            .catch(function(err) {
                console.log(err);
            });
    })
}

module.exports = {
    getQuery: getQuery
};