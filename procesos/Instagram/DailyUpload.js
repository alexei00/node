'strict';
var scraper = require('insta-scraper');
var Promise = require('bluebird');
var async = require('async');
var util = require('../Accounts/Accounts_Instagram');
var mssql = require('../Database/util');
var moment = require('moment');
//Donde se almacena la data
var result = [];
//Donde se alamacena la fecha de la tabla
var date_select = moment().format("YYYY-MM-DD");

//Iterando cada una de los clientes
async.eachSeries(util.accounts_Instagram, function(page, cb) {
    //Getting the information Instagram
    getDataApi(page.nombre).then(function(data) {
        //Getting the information from response
        getData(data).then(function(information) {
            result.push(information);
            cb();
        });
    });
}, function() {
    //Finish, insert the information
    mssqlInsertData().then(function(data) {
        //console.log(data, '****');
    });
});

function getData(data) {
    return new Promise(function(resolve, reject) {
        var obj = {
            id: parseInt(data.id),
            name: data.username,
            posts: parseInt(data.media.count),
            followers: parseInt(data.followed_by.count),
            following: parseInt(data.follows.count)
        };
        resolve(obj);
    });
}

function getDataApi(client) {
    console.log(client, '********');
    return new Promise(function(resolve, reject) {
        scraper.getAccountInfo(client, function(error, response) {
            resolve(response);
        });
    });
}

function mssqlInsertData() {
    return new Promise(function(resolve, reject) {
        async.eachSeries(result, function(row, cb) {
            console.log('Insert data - ', row.name);
            var sql = "IF EXISTS (SELECT * FROM InstagramMedia.dbo.User_Insights WHERE user_id = '" + row.id + "' AND DATE='" + date_select + "') update InstagramMedia.dbo.User_Insights set Media=" + row.posts + ",Follows=" + row.following + ",Followed_by=" + row.followers + " WHERE user_id = '" + row.id + "' AND DATE='" + date_select + "';  ELSE INSERT INTO [InstagramMedia].[dbo].[User_Insights] (Date,User_name,User_id,Media,Follows,Followed_by) values ('" + date_select + "','" + row.name + "','" + row.id + "'," + row.posts + "," + row.followers + "," + row.following + ");";
            console.log(sql);
            var q = mssql.getQuery(sql).then(function(data) {
                cb();
            });
        }, function() {
            console.log('Finish');
            resolve();
        });

    });

};



//INSERT INTO demoTable(someValue) values ('someValue')