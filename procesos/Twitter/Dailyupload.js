'use strict';
var Twitter = require('twitter');
var async = require('async');
var Promise = require('bluebird');
var mssql = require('../Database/util');
var moment = require('moment');

//Tokens app
var TokensAPP = require('../Accounts/Accounts_tokens.js');
var Accounts_Twitter = require('../Accounts/Accounts_Twitter.js');
var client = new Twitter({
    consumer_key: TokensAPP.twitter_tokens.consumer_key,
    consumer_secret: TokensAPP.twitter_tokens.consumer_secret,
    access_token_key: TokensAPP.twitter_tokens.access_token_key,
    access_token_secret: TokensAPP.twitter_tokens.access_token_secret
});
var date_select = moment().subtract(1, 'days').format("YYYY-MM-DD");

//EXTRAYENDO LAS APIS
init();

function init() {
    return new Promise(function(resolve, reject) {
        //Iterando cada una de las cuentas
        async.eachSeries(Accounts_Twitter.accounts_Twitter, function(account, cb) {
            //Creando un objeto temporar para que alamcena la cuenta
            var obj = { user_id: account };
            getDataFromTwitter(obj).then(function(data) {
                GetParams(data[0]).then(function(data_params) {
                    console.log('Getting infomation from - ', data_params.User_name);
                    var sql = "IF EXISTS (SELECT * FROM TwitterAnalytics.dbo.User_Insights WHERE ID = '" + data_params.id + "' AND DATE='" + date_select + "') update TwitterAnalytics.dbo.User_Insights set Followers_count=" + data_params.Followers_count + ",Friends_count=" + data_params.Friends_count + ",Tweets_count=" + data_params.Tweets_count + " WHERE ID = '" + data_params.id + "' AND DATE='" + date_select + "'; ELSE  INSERT INTO TwitterAnalytics.dbo.User_Insights (Date,Screen_name,User_name,ID,Followers_count,Friends_count,Tweets_count) values ('" + date_select + "','"+data_params.Screen_name+"','"+data_params.User_name+"','" + data_params.id + "'," + data_params.Followers_count + "," + data_params.Friends_count + "," + data_params.Tweets_count + ");";
                    //Insertando la información
                //console.log(sql);
                    mssqlInsertData(sql).then(function() {
                        cb();
                    });
                });
            });
        }, function() {
            console.log('--- Finished --');
            resolve();
        });
    });
}


function mssqlInsertData(query) {
    return new Promise(function(resolve, reject) {
        var q = mssql.getQuery(query).then(function(data) {
            resolve();
        });
    });
};




function getDataFromTwitter(params) {
    return new Promise(function(resolve, reject) {
        client.get('users/lookup', params, function(error, data) {
            if (!error) {
                resolve(data);
            } else {
                reject(error);
            }
        });
    });
}


function GetParams(obj) {
    //console.log(obj);
    return new Promise(function(resolve, reject) {
        var obj_temp = {
            id: obj.id,
            User_name: obj.screen_name,
            Followers_count: obj.followers_count,
            Friends_count: obj.friends_count,
            Screen_name:"@"+obj.screen_name.replace(' ',''),
            //favourites_count:obj.favourites_count,
            Tweets_count: obj.statuses_count
        };
        resolve(obj_temp);
    });
}